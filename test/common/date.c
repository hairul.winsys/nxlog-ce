/*
 * This file is part of the nxlog log collector tool.
 * See the file LICENSE in the source root for licensing terms.
 * Website: http://nxlog.org
 * Author: Botond Botyanszki <botond.botyanszki@nxlog.org>
 */

#include "../../src/common/error_debug.h"
#include "../../src/common/date.h"
#include "../../src/core/nxlog.h"

#include <sys/time.h>
#include <time.h>

#define NX_LOGMODULE NX_LOGMODULE_TEST

/* Generated timestamps for various time strings used in the test */
#define TIME_1970_11_02__10_45_18 (26390718000000LL)
#define TIME_1970_11_22__10_45_18 (28118718000000LL)
#define TIME_1970_05_07__17_39_13 (10949953000000LL)
#define TIME_1970_05_07__17_39_13_42 (10949953420000LL)
#define TIME_1970_05_07__17_39_13_42Z (10949953420000LL)
#define TIME_1970_05_07__17_39_13_42_pGMT (10949953420000LL)
#define TIME_1970_05_07__17_39_13_42_p02_00 (10942753420000LL)
#define TIME_2003_08_24__05_14_15_000003_m01_00 (1061705655000003LL)
#define TIME_2003_08_24__05_14_15_000003_p03_00 (1061691255000003LL)
#define TIME_2011_12_06__19_14_15_m01_00 (1323202455000000LL)
#define TIME_2011_12_06__19_14_15_p00_00 (1323198855000000LL)
#define TIME_2011_12_06__19_14_15_p01_00 (1323195255000000LL)
#define TIME_2011_12_06__19_14_15_p02_00 (1323191655000000LL)
#define TIME_2011_12_06__19_14_15_p03_00 (1323188055000000LL)
#define TIME_2011_12_06__19_14_15_p04_00 (1323184455000000LL)
#define TIME_2011_12_06__19_14_15 (1323198855000000LL)
#define TIME_2003_08_24__05_14_15_000000003_m07_00 (1061727255000000LL)
#define TIME_1970_05_07__17_39_13_p02_00 (10942753000000LL)
#define TIME_2011_01_26__20_30_45 (1296073845000000LL)
#define TIME_1970_05_07__17_39_13_42_m00_00 (10949953420000LL)
#define TIME_2011_05_07__07_09_03 (1304752143000000LL)

nxlog_t nxlog;

typedef struct timepair
{
    const char *t1;
    const char *iso;
    const char *result;
} timepair;


static timepair valid_times[] = 
{
    { "Nov  2 10:45:18", "1970-11-02 10:45:18", "Nov  2 10:45:18" },
    { "Nov 2 10:45:18", "1970-11-02 10:45:18", "Nov  2 10:45:18" },
    { "Nov 02 10:45:18", "1970-11-02 10:45:18", "Nov  2 10:45:18" },
    { "Nov 22 10:45:18", "1970-11-22 10:45:18", "Nov 22 10:45:18" },
    { NULL, NULL, NULL },
};

static apr_time_t to_utc_time(apr_time_t timeval)
{
    apr_time_exp_t time_local;
    apr_time_exp_t time_gmt;
    apr_time_exp_lt(&time_local, timeval);
    apr_time_exp_gmt(&time_gmt, timeval);
    return timeval + (time_local.tm_gmtoff - time_gmt.tm_gmtoff) * APR_USEC_PER_SEC;
}

static apr_time_t from_utc_time(apr_time_t timeval)
{
    apr_time_exp_t time_local;
    apr_time_exp_t time_gmt;
    apr_time_exp_lt(&time_local, timeval);
    apr_time_exp_gmt(&time_gmt, timeval);
    return timeval - (time_local.tm_gmtoff - time_gmt.tm_gmtoff) * APR_USEC_PER_SEC;
}

/* gmtoff in minutes */
static apr_time_t to_timezone(apr_time_t timeval, int gmtoff)
{
    return timeval + gmtoff * 60 * APR_USEC_PER_SEC;
}

static void compare_timeval(apr_time_t timeval, const char *timestr)
{
    char tmpbuf[21];

    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    if ( strcmp(timestr, tmpbuf) != 0 )
    {
	nx_abort("%s != %s", timestr, tmpbuf);
    }
}

int main(int argc UNUSED, const char * const *argv, const char * const *env UNUSED)
{
    apr_status_t rv;
    apr_time_t timeval = 0;
    apr_time_t timeval2 = 0;
    const char *teststr = "May  7 17:39:13";
    const char *teststr2 = "May  7 17:39:13 ";
    const char *teststr3 = "May  7 17:39:13 host ";
    const char *isostr = "1970-05-07 17:39:13";
    char isostr_curryear[21];
    char tmpbuf[21];
    char tmpbuf2[21];
    char curryear[5];
    apr_time_t now;
    int i;
    apr_time_exp_t exp;

    apr_cpystrn(isostr_curryear, "XXXX-05-07 17:39:13", sizeof(isostr_curryear));
    ASSERT(apr_time_exp_gmt(&exp, apr_time_now()) == APR_SUCCESS);
    if ( exp.tm_year < 1900 )
    {
	exp.tm_year += 1900;
    }

    apr_snprintf(curryear, sizeof(curryear), "%d", exp.tm_year);
    memcpy(isostr_curryear, curryear, 4);

    // test iso
    ASSERT(nx_date_parse_iso(&timeval, "1970-05-07 17:39:13,42", NULL) == APR_SUCCESS);
    compare_timeval(timeval, "1970-05-07 17:39:13");
    ASSERT(to_utc_time(timeval) == TIME_1970_05_07__17_39_13_42);

    // localtime
    ASSERT(nx_date_parse_iso(&timeval, "1970-05-07 17:39:13.42", NULL) == APR_SUCCESS);
    ASSERT(to_utc_time(timeval) == TIME_1970_05_07__17_39_13_42);
    ASSERT(nx_date_parse_iso(&timeval, "1970-05-07T17:39:13.42", NULL) == APR_SUCCESS);
    ASSERT(to_utc_time(timeval) == TIME_1970_05_07__17_39_13_42);

    // UTC
    ASSERT(nx_date_parse_iso(&timeval, "1970-05-07T17:39:13.42Z", NULL) == APR_SUCCESS);
    ASSERT(nx_date_parse_iso(&timeval2, "1970-05-07 17:39:13.42+GMT", NULL) == APR_SUCCESS);
    ASSERT(timeval == timeval2);

    ASSERT(nx_date_parse_iso(&timeval, "1970-05-07T17:39:13.42+02:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_1970_05_07__17_39_13_42_p02_00);
    ASSERT(nx_date_parse_iso(&timeval, "2003-08-24T05:14:15.000003-01:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2003_08_24__05_14_15_000003_m01_00);
    ASSERT(nx_date_parse_iso(&timeval, "2003-08-24T05:14:15.000003+03:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2003_08_24__05_14_15_000003_p03_00);

    ASSERT(nx_date_parse_iso(&timeval, "2011-12-06T19:14:15-01:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2011_12_06__19_14_15_m01_00);
    ASSERT(nx_date_parse_iso(&timeval, "2011-12-06T19:14:15+00:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2011_12_06__19_14_15_p00_00);
    ASSERT(nx_date_parse_iso(&timeval, "2011-12-06T19:14:15+01:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2011_12_06__19_14_15_p01_00);
    ASSERT(nx_date_parse_iso(&timeval, "2011-12-06T19:14:15+02:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2011_12_06__19_14_15_p02_00);
    ASSERT(nx_date_parse_iso(&timeval, "2011-12-06T19:14:15+03:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2011_12_06__19_14_15_p03_00);
    ASSERT(nx_date_parse_iso(&timeval, "2011-12-06T19:14:15+04:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_2011_12_06__19_14_15_p04_00);

    ASSERT(nx_date_parse_iso(&timeval, "2011-12-06T19:14:15", NULL) == APR_SUCCESS);
    ASSERT(to_utc_time(timeval) == TIME_2011_12_06__19_14_15);

    // wrong format, nanosecond precision not accepted:
    ASSERT(nx_date_parse_iso(&timeval, "2003-08-24T05:14:15.000000003-07:00", NULL) == APR_EBADDATE);

    // test apache
    // 1970-05-07 17:39:13+02:00
    ASSERT(nx_date_parse_apache(&timeval, "07/May/1970:17:39:13 +0200", NULL) == APR_SUCCESS);
    ASSERT(nx_date_parse_iso(&timeval2, "1970-05-07T17:39:13+02:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == TIME_1970_05_07__17_39_13_p02_00);
    ASSERT(timeval == timeval2);
    compare_timeval(to_timezone(from_utc_time(timeval), 2 * 60), "1970-05-07 17:39:13");
    ASSERT(nx_date_parse(&timeval, "26/Jan/2011:20:30:45 +0100", NULL) == APR_SUCCESS);
    compare_timeval(to_timezone(from_utc_time(timeval), 1 * 60), "2011-01-26 20:30:45");

    // test cisco
    ASSERT(nx_date_parse_cisco(&timeval, "Nov 3 14:50:30.403", NULL) == APR_SUCCESS);
    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strcmp("-11-03 14:50:30", tmpbuf + 4) == 0);

    ASSERT(nx_date_parse_cisco(&timeval, "Nov  3 14:50:30.403", NULL) == APR_SUCCESS);
    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strcmp("-11-03 14:50:30", tmpbuf + 4) == 0);

    ASSERT(nx_date_parse_cisco(&timeval, "Nov 13 14:50:30.403", NULL) == APR_SUCCESS);
    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strcmp("-11-13 14:50:30", tmpbuf + 4) == 0);

    // test win
    // 1970-05-07 17:39:13.42-00:00
    ASSERT(nx_date_parse_win(&timeval, "19700507173913.420000-000", NULL) == APR_SUCCESS);
    ASSERT(nx_date_parse_iso(&timeval2, "1970-05-07T17:39:13.42-00:00", NULL) == APR_SUCCESS);
    ASSERT(timeval == timeval2);
    ASSERT(timeval == TIME_1970_05_07__17_39_13_42_m00_00);

    // test timestamp
    ASSERT(nx_date_parse_timestamp(&timeval, "1258531221.650359", NULL) == APR_SUCCESS);
    ASSERT(timeval == 1258531221650359LL);
    ASSERT(nx_date_parse_timestamp(&timeval, "1258531221", NULL) == APR_SUCCESS);
    ASSERT(timeval == 1258531221000000LL);

    now = apr_time_now();
    //printf("GMT now: %lu\n", now / APR_USEC_PER_SEC);
    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), now) == APR_SUCCESS);
    ASSERT(nx_date_parse_iso(&timeval, tmpbuf, NULL) == APR_SUCCESS);

    ASSERT(nx_date_to_iso(tmpbuf2, sizeof(tmpbuf2), timeval) == APR_SUCCESS);
    //printf("%s <=> %s\n", tmpbuf, tmpbuf2);
    ASSERT(strcmp(tmpbuf, tmpbuf2) == 0);
   
    timeval = now;
    for ( i = 0; i < 60 * 24 * 370; i += 2 )
    {
	ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
	ASSERT(nx_date_parse_iso(&timeval2, tmpbuf, NULL) == APR_SUCCESS);
	ASSERT(nx_date_to_iso(tmpbuf2, sizeof(tmpbuf2), timeval2) == APR_SUCCESS);
	if ( strcmp(tmpbuf, tmpbuf2) != 0 )
	{
	    nx_abort("%s != %s", tmpbuf, tmpbuf2);
	}
	timeval += APR_USEC_PER_SEC * 60; // 1 min increment
    }

    ASSERT(nx_date_parse_iso(&timeval, isostr, NULL) == APR_SUCCESS);
    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strcmp(isostr, tmpbuf) == 0);

    timeval2 = timeval;
    ASSERT(nx_date_fix_year(&timeval2) == APR_SUCCESS);

    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval2) == APR_SUCCESS);
    //printf("%s\n", isostr);
    //printf("%s\n", tmpbuf);
    ASSERT(strcmp(isostr + 4, tmpbuf + 4) == 0);

    ASSERT(nx_date_parse_iso(&timeval, "2011-05-07 07:09:3", NULL) == APR_SUCCESS);
    compare_timeval(timeval, "2011-05-07 07:09:03");
    ASSERT(nx_date_parse_iso(&timeval, "2011-05-07 07:9:03", NULL) == APR_SUCCESS);
    compare_timeval(timeval, "2011-05-07 07:09:03");
    ASSERT(nx_date_parse_iso(&timeval, "2011-05-07 7:9:03", NULL) == APR_SUCCESS);
    compare_timeval(timeval, "2011-05-07 07:09:03");
    ASSERT(nx_date_parse_iso(&timeval, "2011-5-7 07:09:03", NULL) == APR_SUCCESS);
    compare_timeval(timeval, "2011-05-07 07:09:03");
    ASSERT(nx_date_parse_iso(&timeval, "2011-5-7 7:9:3", NULL) == APR_SUCCESS);
    compare_timeval(timeval, "2011-05-07 07:09:03");

    // parse_rfc3164
    rv = nx_date_parse_rfc3164(&timeval, teststr, NULL);
    ASSERT(rv == APR_SUCCESS);
    ASSERT(timeval != 0);
    ASSERT(nx_date_to_rfc3164(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strncmp(teststr, tmpbuf, 15) == 0);

    rv = nx_date_parse_rfc3164(&timeval, teststr2, NULL);
    ASSERT(rv == APR_SUCCESS);
    ASSERT(timeval != 0);
    ASSERT(nx_date_to_rfc3164(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strncmp(teststr, tmpbuf, 15) == 0);

    rv = nx_date_parse_rfc3164(&timeval, teststr3, NULL);
    ASSERT(rv == APR_SUCCESS);
    ASSERT(timeval != 0);
    ASSERT(nx_date_to_rfc3164(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strncmp(teststr, tmpbuf, 15) == 0);

    ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
    ASSERT(strcmp(isostr_curryear, tmpbuf) == 0);

    for ( i = 0; valid_times[i].t1 != NULL; i++ )
    {
	rv = nx_date_parse_rfc3164(&timeval, valid_times[i].t1, NULL);
	ASSERT(rv == APR_SUCCESS);
	ASSERT(timeval != 0);
	ASSERT(nx_date_to_iso(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
	apr_cpystrn(isostr_curryear, valid_times[i].iso, sizeof(isostr_curryear));
	memcpy(isostr_curryear, curryear, 4);
	ASSERT(strcmp(isostr_curryear, tmpbuf) == 0);
	ASSERT(nx_date_to_rfc3164(tmpbuf, sizeof(tmpbuf), timeval) == APR_SUCCESS);
	ASSERT(strcmp(valid_times[i].result, tmpbuf) == 0);
    }

    printf("%s:	OK\n", argv[0]);
    return ( 0 );
}
