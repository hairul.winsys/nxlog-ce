:description: This topic describes how to accept log messages from the Linux systemd journal.
:keywords: systemd,parallelization,systemd journal,logging,key value pairs,journal entry,kernel audit,group id,login uid,machine id,json,json format,nxlog
:module: im_systemd
[[im_systemd]]
[desc="Accepts logs from the systemd journal"]
==== Systemd (im_systemd)

Systemd is a Linux initialization system with parallelization capabilities
and dependency-based control logic. Systemd journal is the logging
component of systemd.

The _im_systemd_ module accepts messages from the systemd journal.

NOTE: To enable running the _im_systemd_ module under the `nxlog` user,
      the latter must be added to the `systemd-journal` group. For example,
      this could be the following command:
      +
      `$ sudo gpasswd -a nxlog -g systemd-journal`

[[im_systemd_config]]
===== Configuration
The _im_systemd_ module accepts the following directive in addition to
the <<config_module_common,common module directives>>.

[[im_systemd_config_readfromlast]]
ReadFromLast:: If set to TRUE, this optional boolean directive will read only
new entries from the journal.

include::../../fields-im_systemd.adoc[]

[[im_systemd_config_examples]]
===== Examples

[[im_systemd_example1]]
.Using the im_systemd module to read the systemd journal
====
In this example, {productName} reads the recent journal messages.

.nxlog.conf
[source,config]
----
include::example$im_systemd.conf[tag=manual_include]
----

Below is the sample of a systemd journal message after it has been accepted
by the _im_systemd_ module and converted into JSON format using the
<<xm_json,xm_json>> module.

.Event sample
[source,log]
----
{"Severity":"info","SeverityValue":6,"Facility":"auth","FacilityValue":3,
"Message":"Reached target User and Group Name Lookups.","SourceName":"systemd",
"ProcessID":1,"BootID":"179e1f0a40c64b6cb126ed97278aef89",
"MachineID":"0823d4a95f464afeb0021a7e75a1b693","Hostname":"user",
"Transport":"kernel","EventReceivedTime":"2020-02-05T14:46:09.809554+00:00",
"SourceModuleName":"systemd","SourceModuleType":"im_systemd"}
----
====
