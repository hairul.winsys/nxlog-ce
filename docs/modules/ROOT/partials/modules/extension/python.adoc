:description: This topic explains how to use Python for processing logs with {productName}.
:keywords: log data,python script,debug log,log level,event record,python functions,nxlog
:module: xm_python
[[xm_python]]
[desc="Provides a Python API and supports calling a Python function during processing"]
==== Python (xm_python)

This module provides support for processing {productName} log data with
methods written in the link:https://www.python.org[Python] language. Currently,
only versions 3.x of Python are supported.

The file specified by the <<xm_python_config_pythoncode,PythonCode>> directive
should contain one or more methods which can be called from the
<<config_module_exec,Exec>> directive of any module. See also the
<<im_python,im_python>> and <<om_python,om_python>> modules.

include::../see_modules_by_pkg.adoc[]

The Python script should import the `nxlog` module, and will have access to
the following classes and functions.

// tag::log_functions[]

nxlog.log_debug(msg):: Send the message _msg_ to the internal logger at DEBUG
  log level. This function does the same as the core
  <<core_proc_log_debug,log_debug()>> procedure.
nxlog.log_info(msg):: Send the message _msg_ to the internal logger at INFO
  log level. This function does the same as the core
  <<core_proc_log_info,log_info()>> procedure.
nxlog.log_warning(msg):: Send the message _msg_ to the internal logger at
  WARNING log level. This function does the same as the core
  <<core_proc_log_warning,log_warning()>> procedure.
nxlog.log_error(msg):: Send the message _msg_ to the internal logger at ERROR
  log level. This function does the same as the core
  <<core_proc_log_error,log_error()>> procedure.

// end::log_functions[]

_class_ nxlog.Module:: This class is instantiated by {productName} and can
  be accessed via the <<xm_python_api_logdata_module,LogData.module>>
  attribute. This can be used to set or access variables associated with the
  module (see the <<xm_python_config_examples,example>> below).

[[xm_python_api_logdata]]
_class_ nxlog.LogData:: This class represents an event. It is instantiated
   by {productName} and passed to the method specified by the
   <<xm_python_proc_python_call,python_call()>> procedure.
+
--
delete_field(name)::: This method removes the field _name_ from the event
  record.
field_names()::: This method returns a list with the names of all the fields
  currently in the event record.
get_field(name)::: This method returns the value of the field _name_ in the
  event.
set_field(name, value)::: This method sets the value of field _name_ to
  _value_.

[[xm_python_api_logdata_module]]
module::: This attribute is set to the Module object associated with the
  event.
--

[[xm_python_config]]
===== Configuration

The _xm_python_ module accepts the following directives in addition to the
<<config_module_common,common module directives>>.

[[xm_python_config_pythoncode]]
PythonCode:: This mandatory directive specifies a file containing Python
  code. The <<xm_python_proc_python_call,python_call()>> procedure can be used
  to call a Python function defined in the file. The function must accept an
  <<xm_python_api_logdata,nxlog.LogData>> object as its argument.

include::../../apidoc-xm_python.adoc[]

[[xm_python_config_examples]]
===== Examples

[[xm_python_example1]]
.Using Python for log processing
====
This configuration calls two Python functions to modify each event record. The
`add_checksum()` uses Python's
link:https://docs.python.org/3/library/hashlib.html[hashlib] module to add a
`$ChecksumSHA1` field to the event; the `add_counter()` function adds a
`$Counter` field for non-DEBUG events.

NOTE: The <<pm_hmac,pm_hmac>> module offers a more complete implementation for
      checksumming. See <<lang_stat>> for a native way to add counters.

.nxlog.conf
[source,config]
----
include::example$extension/python/conf/xm_python2.conf[lines=9..-1]
----

.processlogs2.py
[source,python]
----
include::example$extension/python/py/processlogs2.py[]
----
====
