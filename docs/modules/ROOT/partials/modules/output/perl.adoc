:module: om_perl
[[om_perl]]
[desc="Provides a Perl API for saving or forwarding log data"]
==== Perl (om_perl)

include::../extension/perl.adoc[tag=about]

This module makes it possible to execute Perl code in an output module
that can handle the data directly in Perl. See also the
<<im_perl,im_perl>> and <<xm_perl,xm_perl>> modules.

The module will parse the file specified in the
<<om_perl_config_perlcode,PerlCode>> directive when {productName} starts the
module. The Perl code must implement the _write_data_ subroutine which will be
called by the module when there is data to process. This subroutine is called
for each event record and the event record is passed as an argument. To access
event data, the Log::Nxlog Perl module must be included, which provides the
following methods.

NOTE: To use the _om_perl_ module on Windows, a separate Perl
      environment must be installed, such as
      link:http://strawberryperl.com[Strawberry Perl]. Currently, the _om_perl_ module on Windows requires Strawberry Perl {windows-perl-version}.

include::../extension/perl.adoc[tag=log_methods]

get_field(event, key):: Retrieve the value associated with the field
  named _key_. The method returns a scalar value if the key exists and
  the value is defined, otherwise it returns undef.

include::../extension/perl.adoc[tag=pod]

include::../see_modules_by_pkg.adoc[]

[[om_perl_config]]
===== Configuration

The _om_perl_ module accepts the following directives in addition to the
<<config_module_common,common module directives>>.

[[om_perl_config_perlcode]]
PerlCode:: This mandatory directive expects a file containing valid
  Perl code. This file is read and parsed by the Perl interpreter.
+
[NOTE]
====
On Windows, the Perl script invoked by the *PerlCode* directive must define
the Perl library paths at the beginning of the script to provide access to the
Perl modules.

.nxlog-windows.pl
[source,perl]
----
include::example$extension/perl/nxlog-windows.pl[]
----
====

[[om_perl_config_config]]
Config:: This optional directive allows you to pass configuration strings
  to the script file defined by the <<om_perl_config_perlcode, PerlCode>>
  directive. This is a block directive and any text enclosed within
  `<Config></Config>` is submitted as a single string literal to the Perl code.
+
NOTE: If you pass several values using this directive (for example,
separated by the `\n` delimiter) be sure to parse the string correspondingly
inside the Perl code.

[[om_perl_config_call]]
Call:: This optional directive specifies the Perl subroutine to invoke. With
this directive, you can call only specific subroutines from your Perl code.
If the directive is not specified, the default subroutine `write_data` is invoked.

[[om_perl_config_examples]]
===== Examples

[[om_perl_example1]]
.Handling Event Data in om_perl
====
This output module sends events to the Perl script, which simply writes the
data from the `$raw_event` field into a file.

.nxlog.conf
[source,config]
----
include::example$output/perl/om_perl.conf[lines=9..-1]
----

.perl-output.pl
[source,perl]
----
include::example$output/perl/perl-output.pl[lines=4..-1]
----
====
