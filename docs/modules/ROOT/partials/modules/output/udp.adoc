:module: om_udp
[[om_udp]]
[desc="Sends log data via UDP datagrams"]
==== UDP (om_udp)

This module sends log messages as UDP datagrams to the address and
port specified. UDP is the transport protocol of the legacy BSD Syslog
standard as described in RFC 3164, so this module can be particularly
useful to send messages to devices or Syslog daemons which do not
support other transports.

include::../see_modules_by_pkg.adoc[]

[[om_udp_config]]
===== Configuration

The _om_udp_ module accepts the following directives in addition to the
<<config_module_common,common module directives>>.
The <<om_udp_config_host,Host>> directive is required.

[[om_udp_config_host]]
Host:: The module will connect to this IP address or DNS hostname.

[[om_udp_config_port]]
Port:: The module will connect to this port number on the remote
  host. The default is port 514.

'''

[[om_udp_config_localport]]
LocalPort:: This optional directive specifies the local port number of
  the connection. If this is not specified a random high port number
  will be used, which is not always ideal in firewalled network
  environments.

[[om_udp_config_outputtype]]
OutputType:: See the <<config_outputtype,OutputType>> directive in the
  list of common module directives. If this directive is not specified, the
default is <<config_outputtype_dgram,Dgram>>.

[[om_udp_config_sockbufsize]]
SockBufSize:: This optional directive sets the socket buffer size
  (SO_SNDBUF) to the value specified. If this is not set, the
  operating system default is used.

[[om_udp_config_examples]]
===== Examples

.Sending Raw Syslog over UDP
====
This configuration reads log messages from socket and forwards them
via UDP.

.nxlog.conf
[source,config]
----
include::example$om_udp.conf[lines=2..-1]
----
====
