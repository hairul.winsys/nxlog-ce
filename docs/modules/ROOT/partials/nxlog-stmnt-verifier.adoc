*/bin/nxlog-stmnt-verifier*:: This tool can be used to check
  {productName} Language statements. All statements are read from standard
  input and then validated. If a statement is invalid, the tool prints an
  error to standard error and exits non-zero.