REMOVE: tmp/output
REMOVE: tmp/processed

#WIN
#SETENVVAR: PYTHONHOME=/mingw64
#END

RUNPROCESSOR: modules/extension/python/conf/xm_python.conf
COMPAREFILE: tmp/output modules/extension/python/input/input.txt
COMPAREFILE: tmp/processed modules/extension/python/output/result.txt
REMOVE: tmp/output
REMOVE: tmp/processed

RUNPROCESSOR: modules/extension/python/conf/xm_python2.conf
COMPAREFILE: tmp/output modules/extension/python/output/output2.txt
REMOVE: tmp/output

RUNPROCESSOR: modules/extension/python/conf/xm_python-bad1.conf
COMPAREFILE: tmp/output modules/extension/python/output/output-bad1.txt
REMOVE: tmp/output
