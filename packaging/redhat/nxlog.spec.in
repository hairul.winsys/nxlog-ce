# install  redhat-rpm-config for debuginfo package 
%global debug_package %{nil}

Summary:	nxlog is a modular, multi-threaded, high-performance log management solution
Name:		nxlog-ce
Version:	@VERSION@
Release:	1
License:	NXLog Public License
Group:		System Environment/Daemons
Source:		nxlog-ce-%{version}.tar.gz
Vendor:         NXLog Ltd
URL:		http://nxlog.org
#@rhel7@BuildRequires:	apr-devel pcre-devel openssl-devel libdbi-devel libcap-devel gcc libtool glibc-devel perl rpm-build tar make which
#@sles15|opensuse-leap15@BuildRequires: apr-devel pcre-devel libopenssl-devel libdbi-devel libcap-devel gcc libtool glibc-devel perl rpm-build tar make which
#@rhel8|rhel9@BuildRequires:	apr-devel pcre-devel openssl-devel libcap-devel expat-devel gcc libtool glibc-devel libgcc perl perl-ExtUtils-Embed rpm-build tar make which
Prereq(post,preun):	/sbin/install-info
#@rhel7@Requires: shadow-utils, apr >= 1.2, libdbi >= 0.8.1, openssl, pcre, expat, zlib
#@rhel8|rhel9@Requires: shadow-utils, apr >= 1.2, openssl, pcre, expat, zlib
#@sles15|opensuse-leap15@Requires: shadow, libapr1 >= 1.2, pcre, libexpat1, zlib
#BuildArchitectures:	x86_64
BuildRoot:	%{_tmppath}/%{name}-root
Conflicts:	nxlog-ce < %{version}
Conflicts:	nxlog-ce > %{version}

%description

%prep
%setup -q -n nxlog-ce-%{version}

%build
./configure $(EXTRA_CONFIGURE_OPTIONS) --prefix=/usr --libexecdir=/usr/lib --with-cachedir=/var/spool/nxlog --with-configfile=/etc/nxlog/nxlog.conf --with-pidfile=/run/nxlog/nxlog.pid
make
exit 0

%install
make DESTDIR=$RPM_BUILD_ROOT install
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/extension/*.a
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/extension/*.la
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/input/*.a
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/input/*.la
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/processor/*.a
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/processor/*.la
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/output/*.a
rm -f $RPM_BUILD_ROOT/usr/lib/nxlog/modules/output/*.la

strip $RPM_BUILD_ROOT/usr/lib/nxlog/modules/extension/*.so
strip $RPM_BUILD_ROOT/usr/lib/nxlog/modules/input/*.so
strip $RPM_BUILD_ROOT/usr/lib/nxlog/modules/processor/*.so
strip $RPM_BUILD_ROOT/usr/lib/nxlog/modules/output/*.so
strip $RPM_BUILD_ROOT/usr/bin/*

install -d -m 755 $RPM_BUILD_ROOT/usr/lib/systemd/system
install -m 644 packaging/redhat/nxlog.service $RPM_BUILD_ROOT/usr/lib/systemd/system/nxlog.service

#install -d -m 0770 $RPM_BUILD_ROOT/run/nxlog
install -d -m 0770 $RPM_BUILD_ROOT/var/lib/nxlog/cert
install -d -m 0770 $RPM_BUILD_ROOT/var/spool/nxlog
install -d -m 0770 $RPM_BUILD_ROOT/var/log/nxlog
install -d -m 0775 $RPM_BUILD_ROOT/etc
install -d -m 0775 $RPM_BUILD_ROOT/etc/nxlog/
install -d -m 0775 $RPM_BUILD_ROOT/etc/nxlog/nxlog.d
install -m 664 packaging/redhat/nxlog.conf $RPM_BUILD_ROOT/etc/nxlog/nxlog.conf

%clean

#rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-, root, root)
/usr/bin/*
/usr/lib/nxlog/modules/*
/usr/share/doc/nxlog-ce/*
/usr/share/nxlog-ce/*
#@rhel7|rhel8|rhel9@/usr/share/perl5/vendor_perl/Log/Nxlog.pm
#@sles15|opensuse-leap15@/usr/lib/perl5/vendor_perl/5.26.1/Log/Nxlog.pm
%config /usr/lib/systemd/system/nxlog.service
%config /etc/nxlog/nxlog.conf
%dir /etc/nxlog/nxlog.d

#%attr(0770, nxlog, nxlog) /run/nxlog
%attr(0770, nxlog, nxlog) /var/lib/nxlog/cert
%attr(0770, nxlog, nxlog) /var/spool/nxlog
%attr(0770, nxlog, nxlog) /var/log/nxlog

%pre
if ! (command -v systemctl >/dev/null 2>&1 && systemctl --no-pager | grep -q -- -.mount); then
  echo "WARNING: could not find systemd, incompatible system. System stability is not guaranteed."
fi
getent group nxlog >/dev/null || groupadd -r nxlog
getent passwd nxlog >/dev/null || \
    useradd -r -g nxlog -d /var/spool/nxlog -s /sbin/nologin \
    -c "user for the nxlog log managment tool" nxlog

%post
chown -R nxlog:nxlog /var/lib/nxlog
chown -R nxlog:nxlog /etc/nxlog

systemctl daemon-reload
# Only on install not on upgrade
if [ "$1" -eq 1 ]; then
  systemctl enable nxlog
  systemctl start nxlog
fi

%preun
#only on uninstall, not on upgrades.
if [ $1 = 0 ]; then
  systemctl stop nxlog
  systemctl disable nxlog
fi

%postun
# check if this is an upgrade of the package
if [ "$1" -ge 1 ]; then
  # Restart (full stop and start in case of shared object changes) if currently running.
  if systemctl is-active --quiet nxlog; then
    systemctl stop nxlog >/dev/null 2>&1
    systemctl start nxlog >/dev/null 2>&1
  fi
fi

%changelog

