include::attributes.adoc[]

= {productNameCE} Reference Manual
:author: {companyNameFormal}
:doctype: book
:revnumber: {productVersion}
:revdate: {month} {year}
:copyright: Copyright © {author} {year}
:revremark: {copyright}

// tag::guide_include[]

// Perl version for Windows; update when msys2 version changes
:windows-perl-version: 5.28.2.1

include::man_pages.adoc[leveloffset=+1]

include::configuration.adoc[]

include::language.adoc[]

include::modules.adoc[leveloffset=-1]

// end::guide_include[]

// FIXME: manpages for nxlog, nxlog-processor, nxlog-stmnt-verifier
