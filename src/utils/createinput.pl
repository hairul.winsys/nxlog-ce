#!/usr/bin/perl -w

use strict;

die("filename required in argument") unless ( defined($ARGV[0]) );
open(OUT, ">testinput.h") || die("failed to open header for writing");
print OUT "typedef struct testinput_t
{
    const char *str;
    apr_size_t len;
} testinput_t;
";

print OUT "const testinput_t _testinput[] = {\n";

open(IN, $ARGV[0]) || die("couldn't open $ARGV[0]");
my $cnt = 0;
while ( my $line = <IN> )
{
    #chop($line);
    $line =~ s/ mephisto / host  .localdomain.xx /;
    my $len = length($line);
    $line =~ s/\\/\\\\/g;
    $line =~ s/\"/\\\"/g;
    $line =~ s/\n/\\n/g;
    print OUT "    { \"$line\", $len }, \n";
    $cnt++;
}
print OUT "};\n";
print OUT "\nint _testinputsize = $cnt;\n";

close(IN);
