NXLog Community Edition 3.1 Release Notes

0. The original nxlog.conf shipped with Windows packages accidentally omitted including the contents of nxlog.d.
   This is corrected in this release. The change is not automatically applied to locally modified configuration files therefore may
   require you to add the following manually:

   include %CONFDIR%\\*.conf

1. The following steps are required before using Python modules on Windows operating system
    - Download the Python 3.10 installer ( https://www.python.org/downloads/release/python-3107/ )
    - Install Python 3.10
    - Copy 'python310.dll' from Python installation path to 'nxlog-install-dir' as 'libpython3.10.dll'

2. New platforms support: Ubuntu 22.04, Redhat9

NXLog Community Edition 3.0 Release Notes

pm_filter has been removed
Linux packages now ship with systemd units instead of init scripts 

Please make sure you
1. run a backup and
2. test your configuration
before updating to NXLog-CE 3.0

